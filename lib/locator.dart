import 'package:get_it/get_it.dart';
import 'package:routes_app_lib/routes_app_lib.dart';
import 'package:routes_driver_app/core/services/authentication.dart';
import 'package:routes_driver_app/core/services/driver_authentication.dart';
import 'package:routes_driver_app/core/services/profile.dart';
import 'package:routes_driver_app/core/viewmodels/authentication/forgot_password_model.dart';
import 'package:routes_driver_app/core/viewmodels/authentication/login_model.dart';
import 'package:routes_driver_app/core/viewmodels/authentication/phone_number_otp_model.dart';
import 'package:routes_driver_app/core/viewmodels/authentication/signup_model.dart';
import 'package:routes_driver_app/core/viewmodels/authentication/signup_profile_details_model.dart';
import 'package:routes_driver_app/core/viewmodels/authentication/signup_profile_photo_model.dart';

import 'core/services/api.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  locator.registerFactory(() => LoginModel());
  locator.registerFactory(() => ForgotPasswordModel());
  locator.registerFactory(() => SignUpModel());
  locator.registerFactory(() => SignUpProfilePhotoModel());
  locator.registerFactory(() => SignUpDetailsModel());
  locator.registerFactory(() => PhoneNumberOtpModel());
  locator.registerLazySingleton(() => Api());
  locator.registerLazySingleton(() => AuthenticationService());
  locator.registerLazySingleton(() => ProfileService());
  locator.registerLazySingleton(() => DriverAuthenticationService());
  locator.registerLazySingleton(() => UserService());
  locator.registerLazySingleton(() => LocationService());
}