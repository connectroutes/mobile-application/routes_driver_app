import 'package:flutter/material.dart';
import 'package:routes_driver_app/core/services/authentication.dart';
import 'package:routes_driver_app/core/viewmodels/base_model.dart';


import '../../../locator.dart';

class LoginModel extends BaseModel {
  var authenticationService = locator<AuthenticationService>();

  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  bool loading = false;


  String  emailAddress, password;
  LoginModel(){
    emailController.addListener((){
      emailAddress = emailController.text;
    });
    passwordController.addListener((){
      password = emailController.text;
    });
  }

  login(){
    loading = true;
    notifyListeners();
  }

}