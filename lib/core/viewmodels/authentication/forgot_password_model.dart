import 'package:flutter/material.dart';
import 'package:routes_driver_app/core/services/authentication.dart';
import 'package:routes_driver_app/core/services/generated/authentication.pb.dart';
import 'package:routes_driver_app/core/viewmodels/base_model.dart';


import '../../../locator.dart';

class ForgotPasswordModel extends BaseModel {
  var authenticationService = locator<AuthenticationService>();

  TextEditingController emailController = new TextEditingController();
  bool loading = false;
  bool linkSent = false;

  String  emailAddress, password;
  ForgotPasswordModel(){
    emailController.addListener((){
      emailAddress = emailController.text;
    });
  }

  sendLink()async{
    if (emailAddress != null){ //TODO validate email address
      loading = true;
      notifyListeners();

      try{
        await authenticationService.resetPassword(emailAddress);
        linkSent = true;
      } catch(e){
        print(e);
      } finally{
        loading = false;
        notifyListeners();
      }

    }

  }

}