import 'dart:io';
import 'package:flutter/material.dart';
import 'package:routes_driver_app/core/services/authentication.dart';
import 'package:routes_driver_app/core/services/generated/authentication.pbgrpc.dart';
import 'package:routes_driver_app/core/services/profile.dart';
import 'package:routes_driver_app/core/viewmodels/base_model.dart';
import 'package:routes_app_lib/routes_app_lib.dart';


import '../../../locator.dart';

class SignUpDetailsModel extends BaseModel {
  var profileService = locator<ProfileService>();
  var userService = locator<UserService>();
  var authenticationService = locator<AuthenticationService>();



  String otpAuthId;
  bool phoneVerified = false, emailVerified = false, loading = false, isAgeConsentChecked = false, inputValid = false;



  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  TextEditingController fullNameController = new TextEditingController();
  TextEditingController mobileNumberController = new TextEditingController();

  String emailAddress, password, fullName, mobileNumber, gender;

  User user;

  SignUpDetailsModel() {
    mobileNumberController.addListener((){
      mobileNumber = mobileNumberController.text;
      _validateInput();
    });
    emailController.addListener((){
      emailAddress = emailController.text;
      _validateInput();
    });
    fullNameController.addListener((){
      fullName = fullNameController.text;
      _validateInput();
    });
    passwordController.addListener((){
      password = passwordController.text;
      _validateInput();
    });
  }

  _validateInput(){
    if (fullName.contains(" ") && fullName.length > 2 && RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(emailAddress) && password.length >= 6 && mobileNumber.length == 10){
      if (!inputValid){
        inputValid = true;
        notifyListeners();
      }
    } else {
      if (inputValid){
        inputValid = false;
        notifyListeners();
      }
    }
  }

  refreshUser(){
    user = userService.getCurrentUser();
    notifyListeners();
  }
  updateAgeConsent(bool value){
     this.isAgeConsentChecked = value;
     notifyListeners();
  }

  requestOTP(String phoneNumber) async {
    print('requesting otp');

    try{
      await authenticationService.requestOtp(phoneNumber);
    } catch (e){
      print(e);
    }


  }

  Future<SubmitPersonalInfoResponse> submitPersonalInfo()async{

    try{
      loading = true;
      notifyListeners();
      var response = await authenticationService.submitPersonalInfo(mobileNumber, otpAuthId, fullName, emailAddress, password);

      userService.storeUserToken(response.token, response.userData);

      return response;

    } catch(e){
      print(e);
      return null;
    } finally{
      loading = false;
      notifyListeners();
    }

  }

}