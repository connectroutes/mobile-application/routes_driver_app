import 'dart:io';

import 'package:routes_driver_app/core/services/generated/profile.pbgrpc.dart';
import 'package:routes_driver_app/core/services/profile.dart';
import 'package:routes_driver_app/core/viewmodels/base_model.dart';


import '../../../locator.dart';

class SignUpProfilePhotoModel extends BaseModel {
  var profileService = locator<ProfileService>();

  bool loading = false;

  SignUpProfilePhotoModel(){


  }

  uploadProfilePhoto(File file)async{
    loading = true;
    notifyListeners();
    var response = await profileService.uploadDocument(DocumentType.profile_picture, file);
    print(response);
  }


}