import 'dart:io';

import 'package:routes_driver_app/core/services/api.dart';
import 'package:routes_driver_app/core/services/generated/authentication.pbgrpc.dart';
import 'package:routes_driver_app/core/services/generated/profile.pb.dart';
import 'package:routes_driver_app/core/services/generated/profile.pbgrpc.dart';
import 'package:routes_driver_app/locator.dart';
import 'package:image/image.dart';

class ProfileService{

  Api api = locator<Api>();
 Future<UploadDocumentResponse> uploadDocument(DocumentType documentType, File file) async{
    print("upload document");

    Image image = decodeImage(file.readAsBytesSync());
    var bytes = encodeJpg(image);

    Stream<Chunk> sendChunks() async* {
      yield Chunk()..content = [documentType.value]; //send the document type "header"


      for (var byte in bytes) {
        yield Chunk()..content = [byte];
        await Future.delayed(Duration(milliseconds: 200));
      }
    }

    var client = ProfileServiceClient(api.clientChannel);
    return await client.uploadDocument(sendChunks(), options: api.getCallOptions());
  }

}
