import 'package:routes_driver_app/core/services/api.dart';
import 'package:routes_driver_app/core/services/generated/authentication.pbgrpc.dart';
import 'package:routes_driver_app/locator.dart';

class DriverAuthenticationService{

  Api api = locator<Api>();
 Future<RequestOtpResponse> requestOtp(String phoneNumber) async{
    print("request otp");
    RequestOtpRequest request = RequestOtpRequest();
    request.phoneNumber = phoneNumber;

    var client = AuthenticationServiceClient(api.clientChannel);
    return await client.requestOtp(request, options: api.getCallOptions());
  }

}
