///
//  Generated code. Do not modify.
//  source: authentication.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'authentication.pb.dart' as $0;
export 'authentication.pb.dart';

class AuthenticationServiceClient extends $grpc.Client {
  static final _$requestOtp =
      $grpc.ClientMethod<$0.RequestOtpRequest, $0.RequestOtpResponse>(
          '/com.connectroutes.authentication.AuthenticationService/RequestOtp',
          ($0.RequestOtpRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.RequestOtpResponse.fromBuffer(value));
  static final _$submitOtp =
      $grpc.ClientMethod<$0.SubmitOtpRequest, $0.SubmitOtpResponse>(
          '/com.connectroutes.authentication.AuthenticationService/SubmitOtp',
          ($0.SubmitOtpRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.SubmitOtpResponse.fromBuffer(value));
  static final _$submitPersonalInfo = $grpc.ClientMethod<
          $0.SubmitPersonalInfoRequest, $0.SubmitPersonalInfoResponse>(
      '/com.connectroutes.authentication.AuthenticationService/SubmitPersonalInfo',
      ($0.SubmitPersonalInfoRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.SubmitPersonalInfoResponse.fromBuffer(value));
  static final _$updateProfile = $grpc.ClientMethod<$0.UpdateProfileRequest,
          $0.UpdateProfileResponse>(
      '/com.connectroutes.authentication.AuthenticationService/UpdateProfile',
      ($0.UpdateProfileRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.UpdateProfileResponse.fromBuffer(value));
  static final _$resetPassword = $grpc.ClientMethod<$0.ResetPasswordRequest,
          $0.ResetPasswordResponse>(
      '/com.connectroutes.authentication.AuthenticationService/ResetPassword',
      ($0.ResetPasswordRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.ResetPasswordResponse.fromBuffer(value));
  static final _$confirmResetPassword = $grpc.ClientMethod<
          $0.ConfirmResetPasswordRequest, $0.ConfirmResetPasswordResponse>(
      '/com.connectroutes.authentication.AuthenticationService/ConfirmResetPassword',
      ($0.ConfirmResetPasswordRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.ConfirmResetPasswordResponse.fromBuffer(value));

  AuthenticationServiceClient($grpc.ClientChannel channel,
      {$grpc.CallOptions options})
      : super(channel, options: options);

  $grpc.ResponseFuture<$0.RequestOtpResponse> requestOtp(
      $0.RequestOtpRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$requestOtp, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.SubmitOtpResponse> submitOtp(
      $0.SubmitOtpRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$submitOtp, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.SubmitPersonalInfoResponse> submitPersonalInfo(
      $0.SubmitPersonalInfoRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$submitPersonalInfo, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.UpdateProfileResponse> updateProfile(
      $0.UpdateProfileRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$updateProfile, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.ResetPasswordResponse> resetPassword(
      $0.ResetPasswordRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$resetPassword, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.ConfirmResetPasswordResponse> confirmResetPassword(
      $0.ConfirmResetPasswordRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$confirmResetPassword, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }
}

abstract class AuthenticationServiceBase extends $grpc.Service {
  $core.String get $name =>
      'com.connectroutes.authentication.AuthenticationService';

  AuthenticationServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.RequestOtpRequest, $0.RequestOtpResponse>(
        'RequestOtp',
        requestOtp_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.RequestOtpRequest.fromBuffer(value),
        ($0.RequestOtpResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.SubmitOtpRequest, $0.SubmitOtpResponse>(
        'SubmitOtp',
        submitOtp_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.SubmitOtpRequest.fromBuffer(value),
        ($0.SubmitOtpResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.SubmitPersonalInfoRequest,
            $0.SubmitPersonalInfoResponse>(
        'SubmitPersonalInfo',
        submitPersonalInfo_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.SubmitPersonalInfoRequest.fromBuffer(value),
        ($0.SubmitPersonalInfoResponse value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.UpdateProfileRequest, $0.UpdateProfileResponse>(
            'UpdateProfile',
            updateProfile_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.UpdateProfileRequest.fromBuffer(value),
            ($0.UpdateProfileResponse value) => value.writeToBuffer()));
    $addMethod(
        $grpc.ServiceMethod<$0.ResetPasswordRequest, $0.ResetPasswordResponse>(
            'ResetPassword',
            resetPassword_Pre,
            false,
            false,
            ($core.List<$core.int> value) =>
                $0.ResetPasswordRequest.fromBuffer(value),
            ($0.ResetPasswordResponse value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ConfirmResetPasswordRequest,
            $0.ConfirmResetPasswordResponse>(
        'ConfirmResetPassword',
        confirmResetPassword_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.ConfirmResetPasswordRequest.fromBuffer(value),
        ($0.ConfirmResetPasswordResponse value) => value.writeToBuffer()));
  }

  $async.Future<$0.RequestOtpResponse> requestOtp_Pre($grpc.ServiceCall call,
      $async.Future<$0.RequestOtpRequest> request) async {
    return requestOtp(call, await request);
  }

  $async.Future<$0.SubmitOtpResponse> submitOtp_Pre($grpc.ServiceCall call,
      $async.Future<$0.SubmitOtpRequest> request) async {
    return submitOtp(call, await request);
  }

  $async.Future<$0.SubmitPersonalInfoResponse> submitPersonalInfo_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.SubmitPersonalInfoRequest> request) async {
    return submitPersonalInfo(call, await request);
  }

  $async.Future<$0.UpdateProfileResponse> updateProfile_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.UpdateProfileRequest> request) async {
    return updateProfile(call, await request);
  }

  $async.Future<$0.ResetPasswordResponse> resetPassword_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.ResetPasswordRequest> request) async {
    return resetPassword(call, await request);
  }

  $async.Future<$0.ConfirmResetPasswordResponse> confirmResetPassword_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.ConfirmResetPasswordRequest> request) async {
    return confirmResetPassword(call, await request);
  }

  $async.Future<$0.RequestOtpResponse> requestOtp(
      $grpc.ServiceCall call, $0.RequestOtpRequest request);
  $async.Future<$0.SubmitOtpResponse> submitOtp(
      $grpc.ServiceCall call, $0.SubmitOtpRequest request);
  $async.Future<$0.SubmitPersonalInfoResponse> submitPersonalInfo(
      $grpc.ServiceCall call, $0.SubmitPersonalInfoRequest request);
  $async.Future<$0.UpdateProfileResponse> updateProfile(
      $grpc.ServiceCall call, $0.UpdateProfileRequest request);
  $async.Future<$0.ResetPasswordResponse> resetPassword(
      $grpc.ServiceCall call, $0.ResetPasswordRequest request);
  $async.Future<$0.ConfirmResetPasswordResponse> confirmResetPassword(
      $grpc.ServiceCall call, $0.ConfirmResetPasswordRequest request);
}
