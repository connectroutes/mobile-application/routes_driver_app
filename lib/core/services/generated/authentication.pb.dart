///
//  Generated code. Do not modify.
//  source: authentication.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class RequestOtpRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('RequestOtpRequest', package: const $pb.PackageName('com.connectroutes.authentication'), createEmptyInstance: create)
    ..aOS(1, 'phoneNumber')
    ..hasRequiredFields = false
  ;

  RequestOtpRequest._() : super();
  factory RequestOtpRequest() => create();
  factory RequestOtpRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory RequestOtpRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  RequestOtpRequest clone() => RequestOtpRequest()..mergeFromMessage(this);
  RequestOtpRequest copyWith(void Function(RequestOtpRequest) updates) => super.copyWith((message) => updates(message as RequestOtpRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static RequestOtpRequest create() => RequestOtpRequest._();
  RequestOtpRequest createEmptyInstance() => create();
  static $pb.PbList<RequestOtpRequest> createRepeated() => $pb.PbList<RequestOtpRequest>();
  @$core.pragma('dart2js:noInline')
  static RequestOtpRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<RequestOtpRequest>(create);
  static RequestOtpRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get phoneNumber => $_getSZ(0);
  @$pb.TagNumber(1)
  set phoneNumber($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPhoneNumber() => $_has(0);
  @$pb.TagNumber(1)
  void clearPhoneNumber() => clearField(1);
}

class RequestOtpResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('RequestOtpResponse', package: const $pb.PackageName('com.connectroutes.authentication'), createEmptyInstance: create)
    ..aOS(1, 'message')
    ..hasRequiredFields = false
  ;

  RequestOtpResponse._() : super();
  factory RequestOtpResponse() => create();
  factory RequestOtpResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory RequestOtpResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  RequestOtpResponse clone() => RequestOtpResponse()..mergeFromMessage(this);
  RequestOtpResponse copyWith(void Function(RequestOtpResponse) updates) => super.copyWith((message) => updates(message as RequestOtpResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static RequestOtpResponse create() => RequestOtpResponse._();
  RequestOtpResponse createEmptyInstance() => create();
  static $pb.PbList<RequestOtpResponse> createRepeated() => $pb.PbList<RequestOtpResponse>();
  @$core.pragma('dart2js:noInline')
  static RequestOtpResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<RequestOtpResponse>(create);
  static RequestOtpResponse _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get message => $_getSZ(0);
  @$pb.TagNumber(1)
  set message($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMessage() => $_has(0);
  @$pb.TagNumber(1)
  void clearMessage() => clearField(1);
}

class SubmitOtpRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('SubmitOtpRequest', package: const $pb.PackageName('com.connectroutes.authentication'), createEmptyInstance: create)
    ..a<$core.int>(1, 'otp', $pb.PbFieldType.O3)
    ..aOS(2, 'phoneNumber')
    ..hasRequiredFields = false
  ;

  SubmitOtpRequest._() : super();
  factory SubmitOtpRequest() => create();
  factory SubmitOtpRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SubmitOtpRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  SubmitOtpRequest clone() => SubmitOtpRequest()..mergeFromMessage(this);
  SubmitOtpRequest copyWith(void Function(SubmitOtpRequest) updates) => super.copyWith((message) => updates(message as SubmitOtpRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SubmitOtpRequest create() => SubmitOtpRequest._();
  SubmitOtpRequest createEmptyInstance() => create();
  static $pb.PbList<SubmitOtpRequest> createRepeated() => $pb.PbList<SubmitOtpRequest>();
  @$core.pragma('dart2js:noInline')
  static SubmitOtpRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SubmitOtpRequest>(create);
  static SubmitOtpRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get otp => $_getIZ(0);
  @$pb.TagNumber(1)
  set otp($core.int v) { $_setSignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasOtp() => $_has(0);
  @$pb.TagNumber(1)
  void clearOtp() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get phoneNumber => $_getSZ(1);
  @$pb.TagNumber(2)
  set phoneNumber($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPhoneNumber() => $_has(1);
  @$pb.TagNumber(2)
  void clearPhoneNumber() => clearField(2);
}

class SubmitOtpResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('SubmitOtpResponse', package: const $pb.PackageName('com.connectroutes.authentication'), createEmptyInstance: create)
    ..aOS(1, 'otpAuthenticationId')
    ..aOS(2, 'token')
    ..aOS(3, 'userData')
    ..aOB(4, 'isNewUser')
    ..hasRequiredFields = false
  ;

  SubmitOtpResponse._() : super();
  factory SubmitOtpResponse() => create();
  factory SubmitOtpResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SubmitOtpResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  SubmitOtpResponse clone() => SubmitOtpResponse()..mergeFromMessage(this);
  SubmitOtpResponse copyWith(void Function(SubmitOtpResponse) updates) => super.copyWith((message) => updates(message as SubmitOtpResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SubmitOtpResponse create() => SubmitOtpResponse._();
  SubmitOtpResponse createEmptyInstance() => create();
  static $pb.PbList<SubmitOtpResponse> createRepeated() => $pb.PbList<SubmitOtpResponse>();
  @$core.pragma('dart2js:noInline')
  static SubmitOtpResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SubmitOtpResponse>(create);
  static SubmitOtpResponse _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get otpAuthenticationId => $_getSZ(0);
  @$pb.TagNumber(1)
  set otpAuthenticationId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasOtpAuthenticationId() => $_has(0);
  @$pb.TagNumber(1)
  void clearOtpAuthenticationId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get token => $_getSZ(1);
  @$pb.TagNumber(2)
  set token($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasToken() => $_has(1);
  @$pb.TagNumber(2)
  void clearToken() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get userData => $_getSZ(2);
  @$pb.TagNumber(3)
  set userData($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasUserData() => $_has(2);
  @$pb.TagNumber(3)
  void clearUserData() => clearField(3);

  @$pb.TagNumber(4)
  $core.bool get isNewUser => $_getBF(3);
  @$pb.TagNumber(4)
  set isNewUser($core.bool v) { $_setBool(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasIsNewUser() => $_has(3);
  @$pb.TagNumber(4)
  void clearIsNewUser() => clearField(4);
}

class SubmitPersonalInfoRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('SubmitPersonalInfoRequest', package: const $pb.PackageName('com.connectroutes.authentication'), createEmptyInstance: create)
    ..aOS(1, 'fullName')
    ..aOS(2, 'emailAddress')
    ..aOS(3, 'password')
    ..aOS(4, 'phoneNumber')
    ..aOS(5, 'otpAuthenticationId')
    ..hasRequiredFields = false
  ;

  SubmitPersonalInfoRequest._() : super();
  factory SubmitPersonalInfoRequest() => create();
  factory SubmitPersonalInfoRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SubmitPersonalInfoRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  SubmitPersonalInfoRequest clone() => SubmitPersonalInfoRequest()..mergeFromMessage(this);
  SubmitPersonalInfoRequest copyWith(void Function(SubmitPersonalInfoRequest) updates) => super.copyWith((message) => updates(message as SubmitPersonalInfoRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SubmitPersonalInfoRequest create() => SubmitPersonalInfoRequest._();
  SubmitPersonalInfoRequest createEmptyInstance() => create();
  static $pb.PbList<SubmitPersonalInfoRequest> createRepeated() => $pb.PbList<SubmitPersonalInfoRequest>();
  @$core.pragma('dart2js:noInline')
  static SubmitPersonalInfoRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SubmitPersonalInfoRequest>(create);
  static SubmitPersonalInfoRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get fullName => $_getSZ(0);
  @$pb.TagNumber(1)
  set fullName($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasFullName() => $_has(0);
  @$pb.TagNumber(1)
  void clearFullName() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get emailAddress => $_getSZ(1);
  @$pb.TagNumber(2)
  set emailAddress($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasEmailAddress() => $_has(1);
  @$pb.TagNumber(2)
  void clearEmailAddress() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get password => $_getSZ(2);
  @$pb.TagNumber(3)
  set password($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasPassword() => $_has(2);
  @$pb.TagNumber(3)
  void clearPassword() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get phoneNumber => $_getSZ(3);
  @$pb.TagNumber(4)
  set phoneNumber($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasPhoneNumber() => $_has(3);
  @$pb.TagNumber(4)
  void clearPhoneNumber() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get otpAuthenticationId => $_getSZ(4);
  @$pb.TagNumber(5)
  set otpAuthenticationId($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasOtpAuthenticationId() => $_has(4);
  @$pb.TagNumber(5)
  void clearOtpAuthenticationId() => clearField(5);
}

class SubmitPersonalInfoResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('SubmitPersonalInfoResponse', package: const $pb.PackageName('com.connectroutes.authentication'), createEmptyInstance: create)
    ..aOS(1, 'token')
    ..aOS(2, 'userData')
    ..hasRequiredFields = false
  ;

  SubmitPersonalInfoResponse._() : super();
  factory SubmitPersonalInfoResponse() => create();
  factory SubmitPersonalInfoResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SubmitPersonalInfoResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  SubmitPersonalInfoResponse clone() => SubmitPersonalInfoResponse()..mergeFromMessage(this);
  SubmitPersonalInfoResponse copyWith(void Function(SubmitPersonalInfoResponse) updates) => super.copyWith((message) => updates(message as SubmitPersonalInfoResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static SubmitPersonalInfoResponse create() => SubmitPersonalInfoResponse._();
  SubmitPersonalInfoResponse createEmptyInstance() => create();
  static $pb.PbList<SubmitPersonalInfoResponse> createRepeated() => $pb.PbList<SubmitPersonalInfoResponse>();
  @$core.pragma('dart2js:noInline')
  static SubmitPersonalInfoResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SubmitPersonalInfoResponse>(create);
  static SubmitPersonalInfoResponse _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get token => $_getSZ(0);
  @$pb.TagNumber(1)
  set token($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasToken() => $_has(0);
  @$pb.TagNumber(1)
  void clearToken() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get userData => $_getSZ(1);
  @$pb.TagNumber(2)
  set userData($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasUserData() => $_has(1);
  @$pb.TagNumber(2)
  void clearUserData() => clearField(2);
}

class UpdateProfileRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('UpdateProfileRequest', package: const $pb.PackageName('com.connectroutes.authentication'), createEmptyInstance: create)
    ..aOS(1, 'fullName')
    ..aOS(2, 'emailAddress')
    ..aOS(3, 'phoneNumber')
    ..hasRequiredFields = false
  ;

  UpdateProfileRequest._() : super();
  factory UpdateProfileRequest() => create();
  factory UpdateProfileRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory UpdateProfileRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  UpdateProfileRequest clone() => UpdateProfileRequest()..mergeFromMessage(this);
  UpdateProfileRequest copyWith(void Function(UpdateProfileRequest) updates) => super.copyWith((message) => updates(message as UpdateProfileRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static UpdateProfileRequest create() => UpdateProfileRequest._();
  UpdateProfileRequest createEmptyInstance() => create();
  static $pb.PbList<UpdateProfileRequest> createRepeated() => $pb.PbList<UpdateProfileRequest>();
  @$core.pragma('dart2js:noInline')
  static UpdateProfileRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<UpdateProfileRequest>(create);
  static UpdateProfileRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get fullName => $_getSZ(0);
  @$pb.TagNumber(1)
  set fullName($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasFullName() => $_has(0);
  @$pb.TagNumber(1)
  void clearFullName() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get emailAddress => $_getSZ(1);
  @$pb.TagNumber(2)
  set emailAddress($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasEmailAddress() => $_has(1);
  @$pb.TagNumber(2)
  void clearEmailAddress() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get phoneNumber => $_getSZ(2);
  @$pb.TagNumber(3)
  set phoneNumber($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasPhoneNumber() => $_has(2);
  @$pb.TagNumber(3)
  void clearPhoneNumber() => clearField(3);
}

class UpdateProfileResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('UpdateProfileResponse', package: const $pb.PackageName('com.connectroutes.authentication'), createEmptyInstance: create)
    ..aOS(1, 'message')
    ..hasRequiredFields = false
  ;

  UpdateProfileResponse._() : super();
  factory UpdateProfileResponse() => create();
  factory UpdateProfileResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory UpdateProfileResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  UpdateProfileResponse clone() => UpdateProfileResponse()..mergeFromMessage(this);
  UpdateProfileResponse copyWith(void Function(UpdateProfileResponse) updates) => super.copyWith((message) => updates(message as UpdateProfileResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static UpdateProfileResponse create() => UpdateProfileResponse._();
  UpdateProfileResponse createEmptyInstance() => create();
  static $pb.PbList<UpdateProfileResponse> createRepeated() => $pb.PbList<UpdateProfileResponse>();
  @$core.pragma('dart2js:noInline')
  static UpdateProfileResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<UpdateProfileResponse>(create);
  static UpdateProfileResponse _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get message => $_getSZ(0);
  @$pb.TagNumber(1)
  set message($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMessage() => $_has(0);
  @$pb.TagNumber(1)
  void clearMessage() => clearField(1);
}

class ResetPasswordRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('ResetPasswordRequest', package: const $pb.PackageName('com.connectroutes.authentication'), createEmptyInstance: create)
    ..aOS(1, 'email')
    ..hasRequiredFields = false
  ;

  ResetPasswordRequest._() : super();
  factory ResetPasswordRequest() => create();
  factory ResetPasswordRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ResetPasswordRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  ResetPasswordRequest clone() => ResetPasswordRequest()..mergeFromMessage(this);
  ResetPasswordRequest copyWith(void Function(ResetPasswordRequest) updates) => super.copyWith((message) => updates(message as ResetPasswordRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ResetPasswordRequest create() => ResetPasswordRequest._();
  ResetPasswordRequest createEmptyInstance() => create();
  static $pb.PbList<ResetPasswordRequest> createRepeated() => $pb.PbList<ResetPasswordRequest>();
  @$core.pragma('dart2js:noInline')
  static ResetPasswordRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ResetPasswordRequest>(create);
  static ResetPasswordRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get email => $_getSZ(0);
  @$pb.TagNumber(1)
  set email($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasEmail() => $_has(0);
  @$pb.TagNumber(1)
  void clearEmail() => clearField(1);
}

class ResetPasswordResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('ResetPasswordResponse', package: const $pb.PackageName('com.connectroutes.authentication'), createEmptyInstance: create)
    ..aOS(1, 'message')
    ..hasRequiredFields = false
  ;

  ResetPasswordResponse._() : super();
  factory ResetPasswordResponse() => create();
  factory ResetPasswordResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ResetPasswordResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  ResetPasswordResponse clone() => ResetPasswordResponse()..mergeFromMessage(this);
  ResetPasswordResponse copyWith(void Function(ResetPasswordResponse) updates) => super.copyWith((message) => updates(message as ResetPasswordResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ResetPasswordResponse create() => ResetPasswordResponse._();
  ResetPasswordResponse createEmptyInstance() => create();
  static $pb.PbList<ResetPasswordResponse> createRepeated() => $pb.PbList<ResetPasswordResponse>();
  @$core.pragma('dart2js:noInline')
  static ResetPasswordResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ResetPasswordResponse>(create);
  static ResetPasswordResponse _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get message => $_getSZ(0);
  @$pb.TagNumber(1)
  set message($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMessage() => $_has(0);
  @$pb.TagNumber(1)
  void clearMessage() => clearField(1);
}

class ConfirmResetPasswordRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('ConfirmResetPasswordRequest', package: const $pb.PackageName('com.connectroutes.authentication'), createEmptyInstance: create)
    ..aOS(1, 'token')
    ..aOS(2, 'password')
    ..hasRequiredFields = false
  ;

  ConfirmResetPasswordRequest._() : super();
  factory ConfirmResetPasswordRequest() => create();
  factory ConfirmResetPasswordRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ConfirmResetPasswordRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  ConfirmResetPasswordRequest clone() => ConfirmResetPasswordRequest()..mergeFromMessage(this);
  ConfirmResetPasswordRequest copyWith(void Function(ConfirmResetPasswordRequest) updates) => super.copyWith((message) => updates(message as ConfirmResetPasswordRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ConfirmResetPasswordRequest create() => ConfirmResetPasswordRequest._();
  ConfirmResetPasswordRequest createEmptyInstance() => create();
  static $pb.PbList<ConfirmResetPasswordRequest> createRepeated() => $pb.PbList<ConfirmResetPasswordRequest>();
  @$core.pragma('dart2js:noInline')
  static ConfirmResetPasswordRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ConfirmResetPasswordRequest>(create);
  static ConfirmResetPasswordRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get token => $_getSZ(0);
  @$pb.TagNumber(1)
  set token($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasToken() => $_has(0);
  @$pb.TagNumber(1)
  void clearToken() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get password => $_getSZ(1);
  @$pb.TagNumber(2)
  set password($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPassword() => $_has(1);
  @$pb.TagNumber(2)
  void clearPassword() => clearField(2);
}

class ConfirmResetPasswordResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('ConfirmResetPasswordResponse', package: const $pb.PackageName('com.connectroutes.authentication'), createEmptyInstance: create)
    ..aOS(1, 'message')
    ..hasRequiredFields = false
  ;

  ConfirmResetPasswordResponse._() : super();
  factory ConfirmResetPasswordResponse() => create();
  factory ConfirmResetPasswordResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ConfirmResetPasswordResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  ConfirmResetPasswordResponse clone() => ConfirmResetPasswordResponse()..mergeFromMessage(this);
  ConfirmResetPasswordResponse copyWith(void Function(ConfirmResetPasswordResponse) updates) => super.copyWith((message) => updates(message as ConfirmResetPasswordResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ConfirmResetPasswordResponse create() => ConfirmResetPasswordResponse._();
  ConfirmResetPasswordResponse createEmptyInstance() => create();
  static $pb.PbList<ConfirmResetPasswordResponse> createRepeated() => $pb.PbList<ConfirmResetPasswordResponse>();
  @$core.pragma('dart2js:noInline')
  static ConfirmResetPasswordResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ConfirmResetPasswordResponse>(create);
  static ConfirmResetPasswordResponse _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get message => $_getSZ(0);
  @$pb.TagNumber(1)
  set message($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMessage() => $_has(0);
  @$pb.TagNumber(1)
  void clearMessage() => clearField(1);
}

