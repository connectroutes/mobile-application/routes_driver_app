///
//  Generated code. Do not modify.
//  source: profile.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

// ignore_for_file: UNDEFINED_SHOWN_NAME,UNUSED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class DocumentType extends $pb.ProtobufEnum {
  static const DocumentType profile_picture = DocumentType._(0, 'profile_picture');
  static const DocumentType driver_license = DocumentType._(1, 'driver_license');
  static const DocumentType vehicle_document = DocumentType._(2, 'vehicle_document');
  static const DocumentType road_worthiness = DocumentType._(3, 'road_worthiness');

  static const $core.List<DocumentType> values = <DocumentType> [
    profile_picture,
    driver_license,
    vehicle_document,
    road_worthiness,
  ];

  static final $core.Map<$core.int, DocumentType> _byValue = $pb.ProtobufEnum.initByValue(values);
  static DocumentType valueOf($core.int value) => _byValue[value];

  const DocumentType._($core.int v, $core.String n) : super(v, n);
}

class UploadStatusCode extends $pb.ProtobufEnum {
  static const UploadStatusCode Failed = UploadStatusCode._(0, 'Failed');
  static const UploadStatusCode Ok = UploadStatusCode._(1, 'Ok');

  static const $core.List<UploadStatusCode> values = <UploadStatusCode> [
    Failed,
    Ok,
  ];

  static final $core.Map<$core.int, UploadStatusCode> _byValue = $pb.ProtobufEnum.initByValue(values);
  static UploadStatusCode valueOf($core.int value) => _byValue[value];

  const UploadStatusCode._($core.int v, $core.String n) : super(v, n);
}

