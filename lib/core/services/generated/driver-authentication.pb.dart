///
//  Generated code. Do not modify.
//  source: driver-authentication.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class GetVehicleModelsOrMakesRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('GetVehicleModelsOrMakesRequest', package: const $pb.PackageName('com.connectroutes.authentication'), createEmptyInstance: create)
    ..aOS(1, 'make')
    ..hasRequiredFields = false
  ;

  GetVehicleModelsOrMakesRequest._() : super();
  factory GetVehicleModelsOrMakesRequest() => create();
  factory GetVehicleModelsOrMakesRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetVehicleModelsOrMakesRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  GetVehicleModelsOrMakesRequest clone() => GetVehicleModelsOrMakesRequest()..mergeFromMessage(this);
  GetVehicleModelsOrMakesRequest copyWith(void Function(GetVehicleModelsOrMakesRequest) updates) => super.copyWith((message) => updates(message as GetVehicleModelsOrMakesRequest));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetVehicleModelsOrMakesRequest create() => GetVehicleModelsOrMakesRequest._();
  GetVehicleModelsOrMakesRequest createEmptyInstance() => create();
  static $pb.PbList<GetVehicleModelsOrMakesRequest> createRepeated() => $pb.PbList<GetVehicleModelsOrMakesRequest>();
  @$core.pragma('dart2js:noInline')
  static GetVehicleModelsOrMakesRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetVehicleModelsOrMakesRequest>(create);
  static GetVehicleModelsOrMakesRequest _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get make => $_getSZ(0);
  @$pb.TagNumber(1)
  set make($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMake() => $_has(0);
  @$pb.TagNumber(1)
  void clearMake() => clearField(1);
}

class GetVehicleModelsOrMakesResponse extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('GetVehicleModelsOrMakesResponse', package: const $pb.PackageName('com.connectroutes.authentication'), createEmptyInstance: create)
    ..pPS(1, 'vehicleNames')
    ..hasRequiredFields = false
  ;

  GetVehicleModelsOrMakesResponse._() : super();
  factory GetVehicleModelsOrMakesResponse() => create();
  factory GetVehicleModelsOrMakesResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GetVehicleModelsOrMakesResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  GetVehicleModelsOrMakesResponse clone() => GetVehicleModelsOrMakesResponse()..mergeFromMessage(this);
  GetVehicleModelsOrMakesResponse copyWith(void Function(GetVehicleModelsOrMakesResponse) updates) => super.copyWith((message) => updates(message as GetVehicleModelsOrMakesResponse));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GetVehicleModelsOrMakesResponse create() => GetVehicleModelsOrMakesResponse._();
  GetVehicleModelsOrMakesResponse createEmptyInstance() => create();
  static $pb.PbList<GetVehicleModelsOrMakesResponse> createRepeated() => $pb.PbList<GetVehicleModelsOrMakesResponse>();
  @$core.pragma('dart2js:noInline')
  static GetVehicleModelsOrMakesResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GetVehicleModelsOrMakesResponse>(create);
  static GetVehicleModelsOrMakesResponse _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<$core.String> get vehicleNames => $_getList(0);
}

