///
//  Generated code. Do not modify.
//  source: profile.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

const DocumentType$json = const {
  '1': 'DocumentType',
  '2': const [
    const {'1': 'profile_picture', '2': 0},
    const {'1': 'driver_license', '2': 1},
    const {'1': 'vehicle_document', '2': 2},
    const {'1': 'road_worthiness', '2': 3},
  ],
};

const UploadStatusCode$json = const {
  '1': 'UploadStatusCode',
  '2': const [
    const {'1': 'Failed', '2': 0},
    const {'1': 'Ok', '2': 1},
  ],
};

const Chunk$json = const {
  '1': 'Chunk',
  '2': const [
    const {'1': 'content', '3': 1, '4': 1, '5': 12, '10': 'content'},
  ],
};

const UploadDocumentResponse$json = const {
  '1': 'UploadDocumentResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'code', '3': 2, '4': 1, '5': 14, '6': '.com.connectroutes.profile.UploadStatusCode', '10': 'code'},
  ],
};

const UpdateProfileRequest$json = const {
  '1': 'UpdateProfileRequest',
  '2': const [
    const {'1': 'full_name', '3': 1, '4': 1, '5': 9, '10': 'fullName'},
    const {'1': 'email_address', '3': 2, '4': 1, '5': 9, '10': 'emailAddress'},
    const {'1': 'phone_number', '3': 3, '4': 1, '5': 9, '10': 'phoneNumber'},
  ],
};

const UpdateProfileResponse$json = const {
  '1': 'UpdateProfileResponse',
  '2': const [
    const {'1': 'message', '3': 1, '4': 1, '5': 9, '10': 'message'},
  ],
};

