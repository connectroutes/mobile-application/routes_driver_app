///
//  Generated code. Do not modify.
//  source: driver-authentication.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

const GetVehicleModelsOrMakesRequest$json = const {
  '1': 'GetVehicleModelsOrMakesRequest',
  '2': const [
    const {'1': 'make', '3': 1, '4': 1, '5': 9, '10': 'make'},
  ],
};

const GetVehicleModelsOrMakesResponse$json = const {
  '1': 'GetVehicleModelsOrMakesResponse',
  '2': const [
    const {'1': 'vehicle_names', '3': 1, '4': 3, '5': 9, '10': 'vehicleNames'},
  ],
};

