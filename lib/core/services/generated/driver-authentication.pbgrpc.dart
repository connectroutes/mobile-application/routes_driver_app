///
//  Generated code. Do not modify.
//  source: driver-authentication.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'driver-authentication.pb.dart' as $0;
export 'driver-authentication.pb.dart';

class DriverRegistrationServiceClient extends $grpc.Client {
  static final _$getVehicleModelsOrMakes = $grpc.ClientMethod<
          $0.GetVehicleModelsOrMakesRequest,
          $0.GetVehicleModelsOrMakesResponse>(
      '/com.connectroutes.authentication.DriverRegistrationService/GetVehicleModelsOrMakes',
      ($0.GetVehicleModelsOrMakesRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) =>
          $0.GetVehicleModelsOrMakesResponse.fromBuffer(value));

  DriverRegistrationServiceClient($grpc.ClientChannel channel,
      {$grpc.CallOptions options})
      : super(channel, options: options);

  $grpc.ResponseFuture<$0.GetVehicleModelsOrMakesResponse>
      getVehicleModelsOrMakes($0.GetVehicleModelsOrMakesRequest request,
          {$grpc.CallOptions options}) {
    final call = $createCall(
        _$getVehicleModelsOrMakes, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }
}

abstract class DriverRegistrationServiceBase extends $grpc.Service {
  $core.String get $name =>
      'com.connectroutes.authentication.DriverRegistrationService';

  DriverRegistrationServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.GetVehicleModelsOrMakesRequest,
            $0.GetVehicleModelsOrMakesResponse>(
        'GetVehicleModelsOrMakes',
        getVehicleModelsOrMakes_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $0.GetVehicleModelsOrMakesRequest.fromBuffer(value),
        ($0.GetVehicleModelsOrMakesResponse value) => value.writeToBuffer()));
  }

  $async.Future<$0.GetVehicleModelsOrMakesResponse> getVehicleModelsOrMakes_Pre(
      $grpc.ServiceCall call,
      $async.Future<$0.GetVehicleModelsOrMakesRequest> request) async {
    return getVehicleModelsOrMakes(call, await request);
  }

  $async.Future<$0.GetVehicleModelsOrMakesResponse> getVehicleModelsOrMakes(
      $grpc.ServiceCall call, $0.GetVehicleModelsOrMakesRequest request);
}
