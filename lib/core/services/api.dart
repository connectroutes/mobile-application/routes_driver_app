import 'package:grpc/grpc.dart';
import 'package:routes_app_lib/routes_app_lib.dart';
import '../../locator.dart';

class Api{

  var clientChannel = ClientChannel("local.connectroutes.com",
      port: 50051,
      options: ChannelOptions(
        //TODO: Change to secure with server certificates
        credentials: ChannelCredentials.insecure(),
        idleTimeout: Duration(minutes: 1),
      ));

  CallOptions getCallOptions(){
    var userService = locator<UserService>();
    if (userService.getCurrentUser() == null)
      return CallOptions(metadata: {'x-api-key': 'AIzaSyDuNUQSwNLFEbiCeAHyB2ie9tn_2pnyPag'});
    else return CallOptions(metadata: {'x-api-key': 'AIzaSyDuNUQSwNLFEbiCeAHyB2ie9tn_2pnyPag', 'x-session-id': userService.getSessionToken()});
  }
}