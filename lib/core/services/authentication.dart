import 'package:grpc/grpc.dart';
import 'package:routes_driver_app/core/services/api.dart';
import 'package:routes_driver_app/core/services/generated/authentication.pbgrpc.dart';
import 'package:routes_driver_app/locator.dart';

class AuthenticationService{

  Api api = locator<Api>();
 Future<RequestOtpResponse> requestOtp(String phoneNumber) async{
    print("request otp");
    RequestOtpRequest request = RequestOtpRequest();
    request.phoneNumber = phoneNumber;

    var client = AuthenticationServiceClient(api.clientChannel);
    return await client.requestOtp(request, options: api.getCallOptions());
  }

  Future<SubmitOtpResponse> submitOtp(String phoneNumber, int otp) async{
    print("submit otp");
    SubmitOtpRequest request = SubmitOtpRequest();
    request..phoneNumber = phoneNumber
           ..otp = otp;

    var client = AuthenticationServiceClient(api.clientChannel);
    return await client.submitOtp(request, options: api.getCallOptions());
  }


  Future<SubmitPersonalInfoResponse> submitPersonalInfo(String phoneNumber, String otpAuthenticationId, String fullName, String emailAddress, String password) async{
    print("submit personal info");
    SubmitPersonalInfoRequest request = SubmitPersonalInfoRequest();
    request
      ..phoneNumber = phoneNumber
      ..otpAuthenticationId = otpAuthenticationId
      ..fullName = fullName
      ..emailAddress = emailAddress
      ..password = password;

    var client = AuthenticationServiceClient(api.clientChannel);
    return await client.submitPersonalInfo(request, options: api.getCallOptions());
  }

  Future<ResetPasswordResponse> resetPassword(String emailAddress) async{
    ResetPasswordRequest request = ResetPasswordRequest()
        ..email = emailAddress;

    var client = AuthenticationServiceClient(api.clientChannel);
    return await client.resetPassword(request, options: api.getCallOptions());
  }


}
