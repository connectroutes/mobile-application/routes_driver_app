import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:routes_driver_app/ui/views/authentication/forgot_password_view.dart';
import 'package:routes_driver_app/ui/views/authentication/login_register_view.dart';
import 'package:routes_driver_app/ui/views/authentication/login_view.dart';
import 'package:routes_driver_app/ui/views/authentication/phone_number_otp_view.dart';
import 'package:routes_driver_app/ui/views/authentication/signup_profile_details.dart';
import 'package:routes_driver_app/ui/views/authentication/signup_view.dart';
import 'package:routes_driver_app/ui/views/authentication/signup_profile_photo.dart';
import 'package:routes_driver_app/ui/views/home_view.dart';
import 'package:routes_driver_app/ui/views/onboarding_view.dart';

const String initialRoute = "login";

class Router {

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case 'onboarding':
        return CupertinoPageRoute(builder: (_) => OnboardingView());
      case 'home':
        return CupertinoPageRoute(builder: (_) => HomeView());
      case 'login_register':
        return CupertinoPageRoute(builder: (_) => LoginRegisterView());
      case 'forgot_password':
        return CupertinoPageRoute(builder: (_) => ForgotPasswordView());
      case 'signup_profile_photo':
        return CupertinoPageRoute(builder: (_) => SignUpProfilePhotoView());
      case 'signup_profile_details':
        return CupertinoPageRoute(builder: (_) => SignUpProfileDetailsView());
      case 'signup_phone_number_otp_view':
        return CupertinoPageRoute(builder: (_) => PhoneNumberOtpView(settings.arguments as String));
      case 'login':
        return CupertinoPageRoute(builder: (_) => LoginView());
      case 'signup':
        return CupertinoPageRoute(builder: (_) => SignUpView());
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
              body: Center(
                child: Text('No route defined for ${settings.name}'),
              ),
            ));
    }
  }
}