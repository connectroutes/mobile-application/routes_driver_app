import 'package:flutter/material.dart';
import 'package:routes_app_lib/widgets/widgets.dart';
class OnboardingView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final List<Widget> introWidgetsList = <Widget>[
      OnboardingWidget.onboardingScreen(context, "Make Extra Cash", "assets/city_driver.png",
          "Make extra cash by accepting rides from passengers going your route"),
      OnboardingWidget.onboardingScreen(
          context,
          "Book Multiple Seats",
          "assets/people_tax.png",
          "You can book a ride with your colleagues\nand friends by choosing multiple seats"),
      OnboardingWidget.onboardingScreen(context, "Share a Ride", "assets/friends_online.png",
          "Share a ride with other passengers\ngoing your way"),
    ];

    return OnboardingWidget(introWidgetsList, "login_register");

  }
}
