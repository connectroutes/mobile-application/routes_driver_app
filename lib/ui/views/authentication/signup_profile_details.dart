import 'package:flutter/material.dart';
import 'package:routes_app_lib/routes_app_lib.dart';
import 'package:routes_driver_app/core/viewmodels/authentication/signup_profile_details_model.dart';
import 'package:routes_driver_app/ui/views/base_view.dart';

class SignUpProfileDetailsView extends StatefulWidget {
  @override
  _SignUpProfileDetailsViewState createState() =>
      _SignUpProfileDetailsViewState();
}

class _SignUpProfileDetailsViewState extends State<SignUpProfileDetailsView> {
  SignUpDetailsModel signUpDetailsModel;

  @override
  Widget build(BuildContext context) {
    return BaseView<SignUpDetailsModel>(
      builder: (context, model, child) {
        signUpDetailsModel = model;
        return Scaffold(
          body: SafeArea(
            child: Container(
              padding: EdgeInsets.only(left: 22, top: 10, right: 22),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    RoutesAppBar(
                      "Profile Details",
                      rightWidget: Text(
                        "1 of 9",
                        style: TextStyle(color: Theme.of(context).accentColor),
                      ),
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    TabSelectionButton(["Male", "Female"], "Gender", (value) {
                      model.gender = value;
                    }),
                    SizedBox(
                      height: 27,
                    ),
                    RoutesTextField(
                      model.fullNameController,
                      label: "Full Name",
                      hint: "Enter your full name",
                    ),
                    SizedBox(
                      height: 27,
                    ),
                    RoutesTextField(
                      model.emailController,
                      label: "Email",
                      hint: "e.g example@gmail.com",
                      textInputType: TextInputType.emailAddress,
                      suffix: model.user != null
                          ? model.emailVerified
                              ? Text(
                                  "Verified   ",
                                  style: TextStyle(color: SUCCESS_COLOR),
                                )
                              : Text(
                                  "Unverified   ",
                                  style: TextStyle(color: FAILURE_COLOR),
                                )
                          : null,
                    ),
                    SizedBox(
                      height: 27,
                    ),
                    Text(
                      "Mobile Number",
                      style: TextStyle(fontWeight: FontWeight.w500),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    PhoneNumberInputTextField(
                      model.mobileNumberController,
                      disableFocusColor: true,
                      suffix: model.otpAuthId != null
                          ? Text(
                              "Verified  ",
                              style: TextStyle(color: SUCCESS_COLOR),
                            )
                          : null,
                    ),
                    SizedBox(
                      height: 27,
                    ),
                    RoutesDropdownButton(
                      ["Lagos"],
                      "City",
                      "Enter your city",
                      value: "Lagos",
                    ),
                    SizedBox(
                      height: 27,
                    ),
                    RoutesTextField(
                      model.passwordController,
                      label: "Password",
                      hint: "Enter your password",
                      obscureText: true,
                    ),
                    SizedBox(
                      height: 27,
                    ),
                    GestureDetector(
                      onTap: () {
                        model.updateAgeConsent(!model.isAgeConsentChecked);
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Checkbox(
                            value: model.isAgeConsentChecked,
                            onChanged: (value) {
                              model.updateAgeConsent(value);
                            },
                          ),
                          Text(
                            "Click this box if you're 18 years and above",
                            style: TextStyle(fontSize: 12),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 27,
                    ),
                    Button(
                      "Continue",
                      () async {
                        String phoneNumber = "+234${model.mobileNumber}";
                        model.requestOTP(phoneNumber);
                        var authId = await Navigator.pushNamed(
                            context, "signup_phone_number_otp_view",
                            arguments: phoneNumber);

                        if (authId != null){
                            // new user
                            model.otpAuthId = authId;

                            var response = await model.submitPersonalInfo();
                            if (response != null){
                              model.refreshUser();
                              Utilities.showToast(context, "Please verify your email address to continue");
                            }
                        }

                      },
                      loading: model.loading,
                      disabled: !model.inputValid
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}
