import 'package:flutter/material.dart';
import 'package:routes_app_lib/widgets/widgets.dart';
import 'package:routes_driver_app/core/viewmodels/authentication/forgot_password_model.dart';
import 'package:routes_driver_app/ui/views/base_view.dart';
class ForgotPasswordView extends StatelessWidget{

  @override
  Widget build(BuildContext context) {

    return BaseView<ForgotPasswordModel>(
      builder: (context, model, child) => Scaffold(
        body: SafeArea(
          child: Container(
            padding: EdgeInsets.only(left: 22, top: 10, right: 22),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  IconButton(icon: Icon(Icons.arrow_back), onPressed: (){
                    Navigator.pop(context);
                  },),
                  SizedBox(height: 35,),
                  Text("Forgot Password", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
                  SizedBox(height: 41,),
                  ..._buildPage(model)
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  List<Widget>_buildPage(ForgotPasswordModel model){
    if (model.linkSent){
      return [
        Text("A password reset link has been sent to"),
        Text(model.emailAddress, style: TextStyle(fontWeight: FontWeight.bold),)
      ];
    }

    return [
      Text("Enter your email address to receive a link to reset your password."),
      SizedBox(height: 27,),
      RoutesTextField(model.emailController, hint: "e.g example@gmail.com", label: "Email", textInputType: TextInputType.emailAddress,),
      SizedBox(height: 35,),
      Button("Send Link", (){
        model.sendLink();
      }, loading: model.loading,),
    ];
  }
}