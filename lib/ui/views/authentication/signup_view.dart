import 'package:flutter/material.dart';
import 'package:routes_app_lib/widgets/widgets.dart';
import 'package:routes_driver_app/core/viewmodels/authentication/forgot_password_model.dart';
import 'package:routes_driver_app/core/viewmodels/authentication/login_model.dart';
import 'package:routes_driver_app/core/viewmodels/authentication/signup_model.dart';
import 'package:routes_driver_app/ui/views/base_view.dart';

class SignUpAction {
  String title, description, routeTo;
  SignUpAction(this.title, this.description, this.routeTo);
}

class SignUpView extends StatelessWidget {
  final List<SignUpAction> actions = [
    new SignUpAction("Profile Details", "Tell us a bit about yourself",
        "signup_profile_details"),
    new SignUpAction(
        "Profile Photo", "Set up your profile picture", "signup_profile_photo"),
    new SignUpAction(
        "Vehicle Details", "Describe your car", "signup_vehicle_details"),
    new SignUpAction("Driver’s License", "Upload your driver’s license",
        "signup_driver_license"),
    new SignUpAction("Vehicle Registration Documents",
        "Upload road worthiness, and other", "signup_vehicle_documents"),
  ];
  @override
  Widget build(BuildContext context) {
    return BaseView<SignUpModel>(
      builder: (context, model, child) => Scaffold(
        body: SafeArea(
          child: Container(
            padding: EdgeInsets.only(left: 22, top: 10, right: 22),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                GestureDetector(
                  child: Container(
                    child: Icon(Icons.arrow_back),
                    height: 25,
                    width: 25,
                  ),
                  onTap: () {
                    Navigator.pop(context);
                  },
                ),
                SizedBox(
                  height: 20,
                ),
                Text(
                  "Sign up as a driver to start accepting rides",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 27,
                ),
                Text("This will take 5-10 minutes"),
                SizedBox(
                  height: 27,
                ),
                Flexible(
                  child: ListView.separated(
                      itemBuilder: (context, index) {
                        SignUpAction action = actions[index];
                        return Material(
                          color: Colors.transparent,
                          child: InkWell(
                            onTap: () {
                              Navigator.pushNamed(context, action.routeTo);
                            },
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Container(
                                      margin: EdgeInsets.only(
                                          top: 5, bottom: 2, right: 10),
                                      width: 8,
                                      height: 8,
                                      decoration: BoxDecoration(
                                        color: Theme.of(context)
                                            .textTheme
                                            .display1
                                            .color,
                                        borderRadius: BorderRadius.circular(8),
                                      ),
                                    ),
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      mainAxisSize: MainAxisSize.min,
                                      children: <Widget>[
                                        Text(
                                          action.title,
                                          style: TextStyle(
                                              fontSize: 14,
                                              color: Theme.of(context)
                                                  .textTheme
                                                  .display1
                                                  .color,
                                              fontWeight: FontWeight.bold),
                                        ),
                                        SizedBox(
                                          height: 4,
                                        ),
                                        Flexible(
                                            child: Text(
                                          action.description,
                                        )),
                                        SizedBox(
                                          height: 20,
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                Icon(
                                  Icons.keyboard_arrow_right,
                                  color: Theme.of(context).primaryColor,
                                )
                              ],
                            ),
                          ),
                        );
                      },
                      separatorBuilder: (context, index) => Container(
                          margin: EdgeInsets.only(bottom: 15),
                          child: Divider()),
                      itemCount: actions.length),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
