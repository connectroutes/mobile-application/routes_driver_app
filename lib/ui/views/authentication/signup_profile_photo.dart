import 'dart:io';

import 'package:flutter/material.dart';
import 'package:routes_app_lib/widgets/widgets.dart';
import 'package:routes_app_lib/utils/utils.dart';
import 'package:routes_driver_app/core/viewmodels/authentication/signup_profile_photo_model.dart';
import 'package:routes_driver_app/ui/views/base_view.dart';

class SignUpProfilePhotoView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BaseView<SignUpProfilePhotoModel>(
      builder: (context, model, child) => Scaffold(
        body: SafeArea(
          child: Container(
            padding: EdgeInsets.only(left: 22, top: 10, right: 22),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  RoutesAppBar("Profile Photo", rightWidget: Text("2 of 9", style: TextStyle(color: Theme.of(context).accentColor),),),
                  SizedBox(
                    height: 42,
                  ),
                  Text(
                    "Your photo should",
                  ),
                  SizedBox(
                    height: 21,
                  ),
                  ...buildPhotoTips(context),
                  SizedBox(
                    height: 15,
                  ),
                  Center(child: Image.asset("assets/profile_picture_avatar.png", height: 150, width: 150,)),
                  SizedBox(
                    height: 25,
                  ),
                  Button(
                    "Upload Photo",
                    () {
                      Utilities.pickPicture(context, (File file){
                        print("gotcha");
                        print(file);
                        Navigator.pop(context);
                        model.uploadProfilePhoto(file);
                      });
                    },
                    loading: model.loading,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  List<Widget> buildPhotoTips(BuildContext context) {
    const tips = [
      "Show clear visible face",
      "Be without glasses",
      "Be without filter",
      "Have good lighting"
    ];

    return tips
        .map((tip) => Container(
          padding: EdgeInsets.only(bottom: 17),
          child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.only(top: 5, bottom: 2, right: 10),
                    width: 8,
                    height: 8,
                    decoration: BoxDecoration(
                      color: Theme.of(context).textTheme.display1.color,
                      borderRadius: BorderRadius.circular(8),
                    ),
                  ),
                  Text(tip)
                ],
              ),
        ))
        .toList();
  }
}
