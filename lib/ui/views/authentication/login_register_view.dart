
import 'package:flutter/material.dart';
import 'package:routes_app_lib/widgets/widgets.dart';

class LoginRegisterView extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(left: 22, top: 10, right: 22),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text("Welcome to Routes Driver", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
            SizedBox(height: 33,),
            Button("Sign in", (){ //TODO add continue as name from passenger app
              Navigator.of(context).pushNamed("login");
            }),
            SizedBox(height: 29,),
            Button("Register", (){ //TODO delete user data if it exists
              Navigator.of(context).pushNamed("signup");
            }, outlined: true,),
          ],
        ),
      ),
    );
  }
}