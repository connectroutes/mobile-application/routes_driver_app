import 'package:flutter/material.dart';
import 'package:routes_app_lib/widgets/widgets.dart';
import 'package:routes_driver_app/core/viewmodels/authentication/login_model.dart';
import 'package:routes_driver_app/ui/views/base_view.dart';
class LoginView extends StatelessWidget{

  @override
  Widget build(BuildContext context) {

    return BaseView<LoginModel>(
      builder: (context, model, child) => Scaffold(
        body: SafeArea(
          child: Container(
            padding: EdgeInsets.only(left: 22, top: 10, right: 22),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  IconButton(icon: Icon(Icons.arrow_back), onPressed: (){
                    Navigator.pop(context);
                  },),
                  SizedBox(height: 35,),
                  Text("Please Sign in to continue", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
                  SizedBox(height: 41,),
                  RoutesTextField(model.emailController, hint: "e.g example@gmail.com", label: "Email", textInputType: TextInputType.emailAddress,),
                  SizedBox(height: 27,),
                  RoutesTextField(model.passwordController, obscureText: true, hint: "Enter your password", label: "Password",),
                  SizedBox(height: 13,),
                  GestureDetector( onTap: (){
                    Navigator.of(context).pushNamed("forgot_password");
                  }, child: Text("Forgot password?", style: TextStyle(color: Theme.of(context).textTheme.title.color),)),
                  SizedBox(height: 35,),
                  Button("Sign in", (){
                    model.login();
                  }, loading: model.loading,),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}