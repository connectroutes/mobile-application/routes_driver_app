import 'package:flutter/material.dart';
import 'package:routes_app_lib/routes_app_lib.dart';
import 'package:routes_driver_app/core/viewmodels/authentication/phone_number_otp_model.dart';
import 'package:routes_driver_app/locator.dart';
import 'package:routes_driver_app/ui/views/base_view.dart';
import 'signup_profile_details.dart';

class PhoneNumberOtpView extends StatefulWidget {
  String phoneNumber;
  PhoneNumberOtpView(this.phoneNumber);
  @override
  _LoginPhoneNumberOtpState createState() => _LoginPhoneNumberOtpState(phoneNumber);
}

class _LoginPhoneNumberOtpState extends State<PhoneNumberOtpView> {
  String phoneNumber;

  UserService userService = locator<UserService>();
  _LoginPhoneNumberOtpState(this.phoneNumber);

  @override
  Widget build(BuildContext context) {
    return BaseView<PhoneNumberOtpModel>(
      builder: (context, model, widget) => Scaffold(
        body: Container(
          padding: EdgeInsets.only(top: 94, left: 22, right: 22),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "Enter the OTP code sent to\n$phoneNumber",
              ),
              new Padding(padding: EdgeInsets.only(top: 36)),
              OtpInputTextField(model.inputChanged),
              new Padding(padding: EdgeInsets.only(top: 33)),
              Button(
                "Confirm Mobile Number",
                () async {
                  var response = await model.submitOtp(phoneNumber);
                  if (response.isNewUser) {
                    Navigator.pop(context, response.otpAuthenticationId);
                  } else {
                    userService.storeUserToken(response.token, response.userData);
                    Navigator.of(context).pop();
                  }
                },
                disabled: !model.inputValid,
              ),
              new Padding(padding: EdgeInsets.only(top: 29)),
              model.resendCodeEnabled
                  ? GestureDetector(
                    onTap: (){
                      model.resendOtp(phoneNumber);
                    },
                    child: Center(
                      child: Text(
                          "Resend Code",
                          style: TextStyle(
                              color: Theme.of(context).textTheme.title.color),
                        ),
                    ),
                  )
                  : Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text("Resend code in "),
                        TextCountDown(30, model.enableResetCountDown)
                      ],
                    ),
              new Padding(padding: EdgeInsets.only(top: 27)),
              GestureDetector(
                onTap: (){
                  Navigator.of(context).pop();
                },
                child: Center(
                  child: Text(
                    "Change mobile number",
                    style: TextStyle(
                        color: Theme.of(context).textTheme.title.color),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
